import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpClientModule  } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProfessorService {
  private baseURL = `${environment.apiUrl}/professores`;
  private postURL = ` ${environment.apiUrl}/professores/add`;

  constructor(private http: HttpClient) { }

  getProfessoresList(): Observable<any> {
    return this.http.get(this.baseURL, httpOptions);
  }

  createProfessor(professor: Object): Observable<Object> {
    return this.http.post(this.postURL, professor, httpOptions);
  }
}
