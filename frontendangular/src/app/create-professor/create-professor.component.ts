import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Professor } from '../professor';
import { ProfessorService } from '../professor.service';
import {NgForm} from '@angular/forms';
import { formatDate} from '@angular/common';

@Component({
  selector: 'app-create-professor',
  templateUrl: './create-professor.component.html',
  styleUrls: ['./create-professor.component.css']
})
export class CreateProfessorComponent implements OnInit {

  professor: Professor = new Professor();
  submitted = false;
  alertSucess: boolean = false
  alertWarning: boolean = false
 


  constructor(private professorService: ProfessorService, private router: Router) { 
     
  }

  ngOnInit(): void {
    
  }


  onSubmit(f: NgForm){ 
    this.closeAlert() 
    
    this.professorService.createProfessor(this.professor)
      .subscribe(
        data => {
          console.log(data)
          this.alertSucess = true
          f.resetForm()
        },     
        error => {
          console.log(error)  
          this.alertWarning = true
        }) 
 
  }

  closeAlert() 
  {
    this.alertSucess = false;
    this.alertWarning = false;
    
  }


  voltar() {
    this.router.navigate(['/professores']);
  }

}
