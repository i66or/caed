export class Professor {
    id: number;
    nome: string;
    cpf: number;
    dataNascimento: Date;
    sexo: string;
    email: string;
}
