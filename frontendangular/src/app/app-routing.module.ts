import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateProfessorComponent } from './create-professor/create-professor.component';
import { ListProfessorComponent } from './list-professor/list-professor.component';

const routes: Routes = [
  { path: '', redirectTo: 'professores', pathMatch: 'full'},
  { path: 'professores', component: ListProfessorComponent },
  { path: 'professores/add', component: CreateProfessorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
