import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Professor } from '../professor';
import { ProfessorService } from '../professor.service';

@Component({
  selector: 'app-list-professor',
  templateUrl: './list-professor.component.html',
  styleUrls: ['./list-professor.component.css']
})
export class ListProfessorComponent implements OnInit {

  //professores: Professor[];
  professores: Observable<Professor[]>



  constructor(private professorService: ProfessorService, private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData() {
    this.professores = this.professorService.getProfessoresList()
  //this.professores = this.procedureService.getProcedureList();
    /*
    this.professores = [
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
      {id:1, nome: 'Igor M Ribeiro', cpf:10085359661, dataNascimento: new Date(), sexo: 'M', email: 'igorfmr@gmail.com'},
    ]
    */
  }

  createProfessor() {
    this.router.navigate(['professores/add']);
  }


}
