package com.caed.api.services;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import com.caed.api.entities.Professor;
import com.caed.api.repositories.ProfessorRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class ProfessorServiceTest {

    @MockBean
    private ProfessorRepository professorRepository;

    @Autowired
    private ProfessorService professorService;

    private static final String cpf = "10098756543";

    @BeforeEach
    public void setUp() {
        BDDMockito.given(this.professorRepository.findByCpf(Mockito.anyString())).willReturn(new Professor());
        BDDMockito.given(this.professorRepository.save(Mockito.any(Professor.class))).willReturn(new Professor());
    }


    @Test
    public void testBuscarProfessorPorCpf() {
        Optional<Professor> professor = this.professorService.buscarPorCpf(cpf);
        assertTrue(professor.isPresent());
    }

    @Test
    public void testeSalvarProfessor() {
        Professor professor = this.professorService.salvar(new Professor());
        assertNotNull(professor);
    } 



    
}
