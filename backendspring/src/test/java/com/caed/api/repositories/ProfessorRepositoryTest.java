package com.caed.api.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.caed.api.entities.Professor;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;



//Run With@

@SpringBootTest
@ActiveProfiles("test")
public class ProfessorRepositoryTest {
    
    @Autowired
    private ProfessorRepository professorRepository;

    private static final String cpf = "10098765456";

    @BeforeEach
    public void setUp() throws Exception {
        Professor professor = new Professor();
        professor.setNome("Teste");
        professor.setCpf(cpf);
        professor.setDataNascimento(new Date().toString());
        professor.setEmail("teste@gmail.com");
        professor.setSexo("masculino");
        professor.setIdade(25);

        this.professorRepository.save(professor);
    }

    @AfterEach
    public final void tearDown() throws Exception{
        this.professorRepository.deleteAll();
    }

    @Test
    public void testBuscarPorCpf() {
        Professor professor = this.professorRepository.findByCpf(cpf);
        assertEquals(cpf, professor.getCpf());
    }




}
