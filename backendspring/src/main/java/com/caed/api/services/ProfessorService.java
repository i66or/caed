package com.caed.api.services;

import java.util.List;
import java.util.Optional;

import com.caed.api.entities.Professor;

import org.springframework.stereotype.Service;

@Service
public interface ProfessorService {

    /**
     * Retorna um professor de acordo com um cpf informado.
     * 
     * @param cpf
     * @return Optional<Professor>
     */
    Optional<Professor> buscarPorCpf(String cpf);

    /**
     * Retorna um professor de acordo com um cpf informado.
     * 
     * @param cpf
     * @return 
     */
    Professor buscarPorCpf2(String cpf);

    /**
     * Cadastra um novo professor no banco de dados.
     * 
     * @param professor
     * @return Professor
     */
    Professor salvar(Professor professor);

    /**
     * Atualiza um professor existente no banco de dados
     * 
     * @param professor
     * @return Professor ou null se nao existir
     */
    Professor Atualizar(Professor professor);

    /**
     * Retorna uma lista com todos os professores cadastrados
     */
    List<Professor> getAllProfessor();

	
    
}
