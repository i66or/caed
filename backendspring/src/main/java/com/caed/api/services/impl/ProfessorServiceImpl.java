package com.caed.api.services.impl;

import java.util.List;
import java.util.Optional;

import com.caed.api.entities.Professor;
import com.caed.api.repositories.ProfessorRepository;
import com.caed.api.services.ProfessorService;


//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfessorServiceImpl implements ProfessorService {

   // final static Logger log = Logger.getLogger(ProfessorServiceImpl.class);

    @Autowired
    private ProfessorRepository professorRepository;

    @Override
    public Optional<Professor> buscarPorCpf(String cpf) {
    //    log.info("Buscando um professor por seu cnpf:" + cpf);
        return Optional.ofNullable(professorRepository.findByCpf(cpf));
    }

    public Professor buscarPorCpf2(String cpf) {
       // log.info("Buscando um professor por seu cnpf:" + cpf);
        return professorRepository.findByCpf(cpf);
    }

    @Override
    public Professor salvar(Professor professor) {
        //log.info("Salvando um registro de professor no banco de dados:" + professor.toString());
        return this.professorRepository.save(professor);
    }

   
    @Override
    public List<Professor> getAllProfessor() {
       // log.info("Listando todos os professores");
        return this.professorRepository.findAll();
    }

    @Override
    public Professor Atualizar(Professor professor) {
        // TODO Auto-generated method stub
        return null;
    }

    


}