package com.caed.api;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class CaedApplication {

	public static void main(String[] args) {
		//BasicConfigurator.configure();
		SpringApplication.run(CaedApplication.class, args);
	}

}
