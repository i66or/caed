package com.caed.api.producers;


import java.util.Properties;

import com.caed.api.entities.Professor;
import com.google.gson.Gson;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;


public class ProfessorProducer {

    final Logger logger = LoggerFactory.getLogger(ProfessorProducer.class);
     
    public void execute(Professor professor) {
        
        logger.info("kafka: Configuração kafka Producer");
        Properties props = new Properties();
        //props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);


        KafkaProducer<String, Professor> producer = new KafkaProducer<String, Professor>(props);
        logger.info("Kafka: Producer pronto para enviar topic professor-topic...");

        logger.info("Kafka: Dados do Topic a ser enviado " + professor.toString());
        ProducerRecord<String, Professor> record = new ProducerRecord<String, Professor>("professor-topic", "g1", professor);

        producer.send(record);
        logger.info("Kafka: Producer enviado para o serviço em Go...");

        producer.close();
 
    }
    
}
