package com.caed.api.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name = "professor")
public class Professor implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    
    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf")
    private String cpf;

    //@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
    //@Temporal(TemporalType.DATE)
    @Column(name = "data_nascimento")
    private String dataNascimento;

    //@NotBlank
    //@Size(max=1, message = "Por favor, insira M ou F para sexo")
    @Column(name = "sexo")
    private String sexo;


    //@Email
    @Column(name = "email")
    private String email;

    @Column(name = "idade")
    private int idade;

    //@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_CRIACAO")
    private String dataCriacao;

    //@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ALTERACAO")
    private String dataAlteracao;

    public Professor() {}


    
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getNome() {
        return this.nome;
    }

     
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public String getCpf() {
        return this.cpf;
    }

     
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    
    public String getDataNascimento() {
        return this.dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    
    public String getSexo() {
        return this.sexo;
    }


    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    
    public int getIdade() {
        return this.idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    
    public String getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

   
    public String getDataAlteracao() {
        return this.dataAlteracao;
    }

    public void setDataAlteracao(String dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    @PreUpdate
    public void preUpdate() {
        dataAlteracao = new String();
    }

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCriacao = atual.toString();
        dataAlteracao = atual.toString();
    }

 
    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", nome='" + getNome() + "'" +
            ", cpf='" + getCpf() + "'" +
            ", dataNascimento='" + getDataNascimento() + "'" +
            ", sexo='" + getSexo() + "'" +
            ", email='" + getEmail() + "'" +
            ", idade='" + getIdade() + "'" +
            ", dataCriacao='" + getDataCriacao() + "'" +
            ", dataAlteracao='" + getDataAlteracao() + "'" +
            "}";
    }

    
}
