package com.caed.api.validators;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.caed.api.entities.Professor;

public class ProfessorValidator {

    Professor professor;
    List<String> erros;


    public ProfessorValidator(Professor professor) {
        this.professor = professor;
        erros = new ArrayList<String>();

        this.validarNome(professor.getNome());
        this.validarCPF(professor.getCpf());
        this.validarDataNascimento(professor.getDataNascimento());
        this.validarSexo(professor.getSexo());
        this.validarEmail(professor.getEmail());
    }

    public void validarNome(String nome) {
        if (nome.length() < 0 && nome.length() > 100) {
            erros.add("O tamanho do nome deve ser maior que 0 e menor que 100");
        }
    }

    //regex no cpf ou deixar validar com 11 digitos somente??
    public void validarCPF(String cpf) {
        if (cpf.length() != 11) {
            erros.add("O tamanho do cpf deve ser igual a 11");
        }
    }

   

    public void validarDataNascimento(String data_nascimento) {
        String regex = "^[0-3][0-9]/[0-3][0-9]/(?:[0-9][0-9])?[0-9][0-9]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(data_nascimento);

        if (!matcher.matches()) {
            erros.add("A data de nascimento apresenta um formato inválido");
        }
       
    }

    public void validarEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            System.out.println("email invalido");
            erros.add("email informado é invalido");
        }

    }

    public void validarSexo(String sexo) {
        if ((sexo.length() < 1 && sexo.length() > 1)) {
            erros.add("Para o campo sexo, deve-se informar somente um char");
            System.out.println("Para o campo sexo, deve-se informar somente um char");
        }
        else if (!sexo.equals("M")  &&  !sexo.equals("F")) {
            erros.add("Para o campo sexo, deve-se informar M ou F");
            System.out.println("Para o campo sexo, deve-se informar M ou F");
        }
    }

    public Professor getProfessor() {
        return this.professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public List<String> getErros() {
        return this.erros;
    }

    public void setErros(List<String> erros) {
        this.erros = erros;
    }




    
}
