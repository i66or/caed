package com.caed.api.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.caed.api.services.ProfessorService;
import com.caed.api.validators.ProfessorValidator;

import java.util.List;
import java.util.Optional;

import com.caed.api.entities.Professor;
import com.caed.api.producers.ProfessorProducer;

@RestController
@RequestMapping("/professores")
@CrossOrigin(origins = "*") 
public class ProfessorController {

   final Logger logger = LoggerFactory.getLogger(ProfessorController.class);

   @Autowired
   private ProfessorService professorService;
      
    @GetMapping()
    public List<Professor> getAllProfessor() {
        logger.info("Request: GET professores");
        List<Professor> professores = professorService.getAllProfessor();
        return professores;
    }
    @PostMapping(path = "/add")
    public ResponseEntity<String> addProfessor(@RequestBody Professor professor) {
        logger.info("Resquest: post professor");       

        ProfessorValidator validator = new ProfessorValidator(professor);
        if (validator.getErros().size() > 0) {
            logger.error("Request: Erro 500 -> professor não foi validado");
           // return new ResponseEntity<String>("GET Response", HttpStatus.OK);
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            
        }
        logger.info("Request: Ok 200 ->  professor foi validado");

        ProfessorProducer producer = new ProfessorProducer();
        producer.execute(professor);
        logger.info("Request: enviado para o serviço externo para persistência");

        return new ResponseEntity<>(HttpStatus.OK);     
    }


    public Professor buscarPorCpf2(Professor professor) {
        return professorService.buscarPorCpf2(professor.getCpf());
    }
    
    @GetMapping(value = "/cpf/{cpf}")
    public ResponseEntity<String> buscarPorCpf(@PathVariable("cpf") String cpf) {
        logger.info("Request: GET professor por CPF");
        Optional<Professor> professor = professorService.buscarPorCpf(cpf);

        if (!professor.isPresent()) {
           logger.info("Professor não encontrado pelo cpf informado:" + cpf);
           return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        logger.info("Professor encontrado pelo cpf informado: " + cpf);
        return new ResponseEntity<>(HttpStatus.OK);
    }    
}
