package com.caed.api.repositories;

import com.caed.api.entities.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = false)
public interface ProfessorRepository extends JpaRepository<Professor, Long>{

    
    Professor findByCpf(String cpf);
    
}
