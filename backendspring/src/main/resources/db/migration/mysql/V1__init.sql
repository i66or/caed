CREATE TABLE `professor` (
    `id` bigint(20) NOT NULL ,
    `nome` varchar(255) NOT NULL, 
    `cpf` varchar(255) NOT NULL,
    `data_nascimento` varchar(255) NOT NULL,
    `sexo` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,    
    `idade` varchar(255) NOT NULL,
    `data_criacao` varchar(255) NOT NULL,
    `data_alteracao` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `professor`
  ADD PRIMARY KEY (`id`);

  ALTER TABLE `professor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;