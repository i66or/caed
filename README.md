# Prova Prática
 EDITAL PROCESSO SELETIVO No 001/2021 ÁREA DE DESENVOLVIMENTO DE TECNOLOGIA - DESENVOLVEDOR FULL STACK



# Pré-requisitos:
Pré-requisitos para instalação:

  - instalar docker 
  - instalar docker-compose



### Instalação

Executar o comando no diretório raiz do projeto:

```sh
$ cd caed
$ docker-compose up -d
```

as seguintes mensagens serão exibidas no caso da instalação ser completada com sucesso:

```sh
$ Creating caed_zookeeper_1     ... done
$ Creating caed_elasticsearch_1 ... done
$ Creating caed_db_1            ... done
$ Creating caed_logstash_1      ... done
$ Creating caed_kafka_1         ... done
$ Creating caed_kibana_1        ... done
$ Creating caed_go_1            ... done
$ Creating caed_backendspring_1 ... done
$ Creating caed_frontendangular_1 ... done
```

### Descrição 

O comando descrito acima irá executar a inicialização e instalação de todos os containers no arquivo docker-compose.yml, localizado na raiz do projeto, que são:

- MySQL: faz o build do bd e disponibiliza o acesso através da porta 3306;

- Front-end Angular: faz o build do frontend e disponibiliza o resultado de forma estática para o web-server nginx na porta 80;

- Zookeeper e Kafka: fazem o build do kafka e zooperkeeper nas portas 2181 e 9092 respectivamente e disponibiliza a conexão com o kafka na porta 9092;
 
- Back-end Spring: faz o build da aplicação REST e disponibiliza os endpoints Post e Get para inserir e listar os registros. Cria as tabelas do banco MySQL na porta 3306. Cria um banco H2 para execução de teste. Cria o producer, gera um topic e envia para o broker kafka na porta 9092. Gera logs através do logstash na porta 5009;

- Go: faz o build do microserviço em Go, que é disponibilizado na porta 1000. É responsável pelo Consumer dos topicos do kafka na porta 9092. Aplica as regras e faz a persistência no banco de dados MySQL na porta 3306;

- Kibana: Logs gerados no beckend podem ser visualizados com o kibana através da porta 5601 .

### Execução

Depois de executar os comandos da seção de instalação e verificar se todos os containers estão online, aguarde alguns segundos e os seguintes endpoints ficarão disponíveis:

GET

```sh
localhost:80/api/professores
```
POST

```sh
localhost:80/api/professores/add
```

acessados através de:
```sh
http://localhost/professores
```
Screenshot:https://bitbucket.org/i66or/caed/src/master/screens/Lisategem.PNG


```sh
http://localhost/professores/add
```
Screenshot:https://bitbucket.org/i66or/caed/src/master/screens/Cadastro.PNG


---
**NOTE**
IMPORTANTE: A instalação foi testada e funciona corretamente em distribuições Linux. Se durante a instalação ocorrer um erro como:

```sh
$ /bin/sh: ./mvnw: not found
$ ERROR: Service 'backendspring' failed to build: The command '/bin/sh -c ./mvnw ins all -DskipTests' returned a non-zero code: 127
```
substitua a pasta "backendspring" pela pasta disponibilizada em:
https://www.dropbox.com/sh/ycgeq0aw5ktp3qg/AABjkWrKq5UliFkBIYY4mkTxa?dl=0

e execute:
```sh
$ docker-compose up -d --build --force-recreate
```

---




