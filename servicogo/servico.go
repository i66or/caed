package main

import (
	"fmt"
	"log"
	"time"

	//"os"
	//"strconv"
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/segmentio/kafka-go"

	//"github.com/gorilla/mux"
	age "github.com/bearbin/go-age"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	//"database/sql"
	//"github.com/go-sql-driver/mysql"
)

type Professor struct {
	Id             int    `json:"id"`
	Nome           string `json:"nome"`
	Cpf            string `json:"cpf"`
	DataNascimento string `json:"dataNascimento"`
	Sexo           string `json:"sexo"`
	Email          string `json:"email"`
	Idade          int    `json:"idade"`
	DataCriacao    string `json:"dataCriacao"`
	DataAlteracao  string `json:"dataAlteracao"`
}

const (
	//banco
	username = "root"
	password = "caed"
	hostname = "db:3306"
	dbname   = "caed"

	// layout para formatar datas
	layoutCustom = "01/Jan/2006 15:04:05"

	// kafka
	topic         = "professor-topic"
	brokerAddress = "kafka:9092"
	groupId       = "g1"
)

var Professores []Professor

/*------------------ database -------------------------- */

var db *gorm.DB

func initDB() {
	var err error
	dataSourceName := "root:caed@tcp(db:3306)/caed?parseTime=True"
	db, err = gorm.Open("mysql", dataSourceName)

	if err != nil {
		fmt.Println(err)
		panic("falha ao conectar ao banco de dados...")
	} else {
		fmt.Println("Conectado ao banco de dados com sucesso!")

	}
}

func save(p Professor) {

	var professor Professor
	//verificar se existe um professor pelo cpf
	db.Table("professor").Where("cpf = ?", p.Cpf).First(&professor)
	if professor.Cpf == "" {
		//salva

		fmt.Printf("date is :%s\n", p.DataNascimento)

		dia, _ := strconv.Atoi(string(p.DataNascimento[0:2]))
		mes, _ := strconv.Atoi(string(p.DataNascimento[3:5]))
		ano, _ := strconv.Atoi(string(p.DataNascimento[6:10]))

		idade := age.Age(time.Date(ano, time.Month(mes), dia, 0, 0, 0, 0, time.UTC))
		fmt.Printf("idade :%d\n", idade)

		p.Idade = idade
		p.DataCriacao = time.Now().Format(layoutCustom)
		p.DataAlteracao = time.Now().Format(layoutCustom)

		result := db.Table("professor").Create(&p)
		fmt.Println(result.Error)

	} else {
		//Atualiza
		fmt.Println("Professor Já existe no banco de dados")
		p.Id = professor.Id

		dia, _ := strconv.Atoi(string(p.DataNascimento[0:2]))
		mes, _ := strconv.Atoi(string(p.DataNascimento[3:5]))
		ano, _ := strconv.Atoi(string(p.DataNascimento[6:10]))

		idade := age.Age(time.Date(ano, time.Month(mes), dia, 0, 0, 0, 0, time.UTC))
		fmt.Printf("idade :%d\n", idade)

		p.Idade = idade

		p.DataCriacao = professor.DataCriacao
		p.DataAlteracao = time.Now().Format(layoutCustom)
		result := db.Model(&p).Table("professor").Where("id = ?", p.Id).Updates(Professor{Nome: p.Nome, Cpf: p.Cpf, DataNascimento: p.DataNascimento, Sexo: p.Sexo, Idade: p.Idade, Email: p.Email, DataCriacao: p.DataCriacao, DataAlteracao: p.DataAlteracao})
		fmt.Println(result.Error)

	}
}

/* --------------------------kafka -------------------------- */
func StartKafka() {
	conf := kafka.ReaderConfig{
		Brokers:  []string{brokerAddress},
		Topic:    topic,
		GroupID:  groupId,
		MaxBytes: 10,
	}

	r := kafka.NewReader(conf)

	for {
		m, err := r.ReadMessage(context.Background())
		if err != nil {
			fmt.Println("ocorreu um erro", err)
			continue
		}
		fmt.Println("Mensagem recebida: ", string(m.Value))

		var p Professor
		e := json.Unmarshal([]byte(m.Value), &p)
		if err != nil {
			log.Fatalf("Ocorreu um erro durante o parser do Json para a struct professor. Error: %s", e.Error())
		}
		fmt.Printf("p Struct: %#v\n", p)

		save(p)

	}
}

/*------------------roteamento -------------------------- */

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "HomePage")
	fmt.Println("Endpoint : HomePage")
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	log.Fatal(http.ListenAndServe(":1000", nil))
}

/*------------------ main -------------------------- */

func main() {

	//handleRequests()
	fmt.Println("Serviço em Go inicializado...")
	//os.Setenv("TZ", "AMERICA/Sao_Paulo")
	initDB()
	StartKafka()
	fmt.Println("Kafka consumer foi inicializado...")
	time.Sleep(10 * time.Minute)

	//initDB()
	//save()

}
