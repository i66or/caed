module servicogo

go 1.15

require (
	github.com/bearbin/go-age v0.0.0-20140407072555-316d0c1e7cd1 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/segmentio/kafka-go v0.4.9 // indirect
)
